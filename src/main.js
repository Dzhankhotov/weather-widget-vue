import { defineCustomElement } from 'vue'
import App from './App.ce.vue';

customElements.define('weather-widget', defineCustomElement(App));

// BELOW IS AN ALTERNATIVE CUSTOM SOLUTION, WHICH WOULD ALLOW INJECTING PINIA STORE, HOWEVER,
// DOESN'T LOAD STYLES (SEEMS VITE IS DISCONNECTED).

// import { createApp, defineCustomElement, getCurrentInstance, h } from 'vue'
// import { createPinia } from 'pinia';
// import App from './App.ce.vue';
// import { library } from '@fortawesome/fontawesome-svg-core';
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
// import { faUserSecret } from '@fortawesome/free-solid-svg-icons';
// import Draggable from 'vuedraggable';

// const pinia = createPinia();

// const config = {
//     component: App,
//     sharedStoreInstance: true,
//     plugins: [pinia],
//     renderOptions: { ref: 'component' }
// }

// const createElementInstance = ({ component = null, props = [], sharedStoreInstance = false, plugins = [], renderOptions = {} } = {}) => {
//     return defineCustomElement({
//         props: props,
//         setup() {
//             const app = createApp();

//             if (!sharedStoreInstance) {
//                 const pinia = createPinia();
//                 app.use(pinia);
//             }

//             plugins.forEach(plugin => app.use(plugin));

//             library.add(faUserSecret);
//             app.component('FAIcon', FontAwesomeIcon);

//             app.component('draggable', Draggable, {});

//             const inst = getCurrentInstance();
//             Object.assign(inst.appContext, app._context);
//             Object.assign(inst.provides, app._context.provides);
//         },
//         render: () => h(component, renderOptions)
//     });
// }

// customElements.define('weather-widget', createElementInstance(config));



// import { createApp, defineCustomElement } from 'vue'
// import { createPinia } from 'pinia'
// import AppCE from './App.ce.vue'
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
// import Draggable from 'vuedraggable'

// // import './assets/css/weather-icons.css'
// // import './assets/css/weather-icons-wind.css'
// // import './assets/scss/styles.scss'

// const app = createApp(AppCE)

// app.use(createPinia())
// library.add(faUserSecret)
// // app.component('FAIcon', FontAwesomeIcon)

// // app.component('draggable', Draggable, {})

// // app.mount('#app')

// customElements.define('weather-widget', defineCustomElement(AppCE))
