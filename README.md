# Weather wiget Vue

## Project Setup

```sh
npm i
```

Create `.env` file in root folder with following content:
```bash
VITE_OWM_APIKEY={YOUR_API_KEY_FOR_OPENWEATHERMAP_WEATHER_DATA}
```

API key can be obtained from [OpenWeatherMap website](https://openweathermap.org/api)

### Testing the project
Run

```sh
npm run build && npm run dev
```

and navigate to local address in browser.
